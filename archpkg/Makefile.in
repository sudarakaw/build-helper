# @configure_input@
#
# Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
#
# This program comes with ABSOLUTELY NO WARRANTY;
# This is free software, and you are welcome to redistribute it and/or modify it
# under the terms of the BSD 2-clause License. See the LICENSE file for more
# details.
#

SED					= @SED@
SHA256SUM			= @SHA256SUM@
MAKEPKG				= @MAKEPKG@
MAKEPKGFLAGS		= @MAKEPKGFLAGS@
PKGARCH				= @PKGARCH@
PKGRELEASE			= @PKGRELEASE@
PKGFILE				= $(pkgdir)/$(distdir)-$(PKGRELEASE)-$(PKGARCH).pkg.tar.zst
PKGMAINTAINER		= @PKGMAINTAINER@
PACKAGE_BUGREPORT	= @PACKAGE_BUGREPORT@
NAMCAP				= @NAMCAP@

.PHONY: archpkg
archpkg: $(PKGFILE)
ifneq ($(NAMCAP),notfound)
	$(NAMCAP) $<
endif

$(PKGFILE): $(tarfile) FORCE_PKG
	$(SED) -i "s/^\(sha256sums=(\).*\()\)$$/\1'`$(SHA256SUM) $< | cut -d' ' -f1`'\2/" $(pkgdir)/PKGBUILD
	cd $(pkgdir) && PACKAGER="$(PKGMAINTAINER) <$(PACKAGE_BUGREPORT)>" $(MAKEPKG) $(MAKEPKGFLAGS)


.PHONY: FORCE_PKG
FORCE_PKG:
	-$(RM) $(PKGFILE) >/dev/null 2>&1
	-$(RM) $(PKGFILE).sig >/dev/null 2>&1


.PHONY: archpkgdist
archpkgdist:
	$(MKDIR_P) $(distdir)/$(pkgdir)
	$(CP) $(pkgdir)/PKGBUILD.in $(distdir)/$(pkgdir)


.PHONY: archpkgdistclean
archpkgdistclean:
	$(RM) $(pkgdir)/PKGBUILD


