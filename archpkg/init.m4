dnl  archpkg/init.m4: Arch Linux packaging
dnl
dnl  Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify it
dnl  under the terms of the BSD 2-clause License. See the LICENSE file for more
dnl  details.
dnl
dnl  Process this file with autoconf to produce a configure script.
dnl

AC_DEFUN([USE_ARCHPKG], [
  INCLUDE_MAKEFILE([archpkg/Makefile])

  AC_CONFIG_FILES([${pkgdir}/PKGBUILD])

  AC_PATH_PROG([UNAME], [uname], [notfound])
  AS_IF([test "X$UNAME" = "Xnotfound"],
        [AC_MSG_ERROR([uname executable not found.])])

  AC_PATH_PROG([MAKEPKG], [makepkg], [notfound])
  AS_IF([test "X$MAKEPKG" = "Xnotfound"],
        [AC_MSG_ERROR([makepkg executable not found.])])

  AC_PATH_PROG([SED], [sed], [notfound])
  AS_IF([test "X$SED" = "Xnotfound"],
        [AC_MSG_ERROR([sed executable not found.])])

  AC_PATH_PROG([SHA256SUM], [sha256sum], [notfound])
  AS_IF([test "X$SHA256SUM" = "Xnotfound"],
        [AC_MSG_ERROR([sha256sum executable not found.])])

  AC_PATH_PROG([NAMCAP], [namcap], [notfound])

  PKGARCH=m4_esyscmd_s([uname -m])
  AC_SUBST([PKGARCH])

  PKGMAINTAINER="m4_esyscmd_s([git config --get user.name])"
  AC_SUBST([PKGMAINTAINER])

  AC_ARG_WITH([release],
                [AS_HELP_STRING([--with-release],
                                [Binary distribution release (default: 1)])],
                [release=${withval}],
                [release=1])
  PKGRELEASE=${release}
  AC_SUBST([PKGRELEASE])

  AC_ARG_ENABLE([pkgsign],
              [AS_HELP_STRING([--disable-pkgsign],
                              [Do not generate signature file for Arch Linux package])],
              [PKGSIGN=no],
              [PKGSIGN=yes])

  MAKEPKGFLAGS="--clean"
  AC_SUBST([MAKEPKGFLAGS])
  AS_IF([test "X$DEBUG" = "Xno"],
        [MAKEPKGFLAGS="${MAKEPKGFLAGS} --cleanbuild"],
        [MAKEPKGFLAGS="${MAKEPKGFLAGS} --skipchecksums"])

  AS_IF([test "X$PKGSIGN" = "Xyes"],
        [MAKEPKGFLAGS="${MAKEPKGFLAGS} --sign"])

  REGISTER_DIST([archpkg])
  REGISTER_DISTCLEAN([archpkg])

  MODULE_RESULT([Packaging])
  MODULE_RESULT([  Release         : ${PKGRELEASE}])
  MODULE_RESULT([  Sign package    : ${PKGSIGN}])
  MODULE_RESULT([])
])
