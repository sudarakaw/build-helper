dnl  build/init.m4: initialize generic build target
dnl
dnl  Copyright 2021 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify it
dnl  under the terms of the BSD 2-clause License. See the LICENSE file for more
dnl  details.
dnl
dnl  Process this file with autoconf to produce a configure script.
dnl

AC_DEFUN([MAKE_BUILD], [
  INCLUDE_MAKEFILE([build/Makefile])

  BUILD_REGISTERED_TARGETS=""
  AC_SUBST([BUILD_REGISTERED_TARGETS])

  WATCH_REGISTERED_TARGETS=""
  AC_SUBST([WATCH_REGISTERED_TARGETS])
])

AC_DEFUN([REGISTER_BUILD], [
  AS_IF([test "X[$1]" != "X"],
        [BUILD_REGISTERED_TARGETS="${BUILD_REGISTERED_TARGETS} [$1]build"])
])
