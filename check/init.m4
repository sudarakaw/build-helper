dnl  check/init.m4: initialize check target
dnl
dnl  Copyright 2021 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify it
dnl  under the terms of the BSD 2-clause License. See the LICENSE file for more
dnl  details.
dnl
dnl  Process this file with autoconf to produce a configure script.
dnl

AC_DEFUN([MAKE_CHECK], [
  INCLUDE_MAKEFILE([check/Makefile])

  AC_ARG_ENABLE([build-and-check],
    [AS_HELP_STRING([--enable-build-and-check],
      [Run build target before check])],
    [CHECK_REGISTERED_TARGETS=build],
    [CHECK_REGISTERED_TARGETS=""])

  AC_SUBST([CHECK_REGISTERED_TARGETS])
])

AC_DEFUN([REGISTER_CHECK], [
  AS_IF([test "X[$1]" != "X"],
        [CHECK_REGISTERED_TARGETS="${CHECK_REGISTERED_TARGETS} [$1]check"])
])
