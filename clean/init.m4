dnl  clean/init.m4: initialize clean target
dnl
dnl  Copyright 2021 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify it
dnl  under the terms of the BSD 2-clause License. See the LICENSE file for more
dnl  details.
dnl
dnl  Process this file with autoconf to produce a configure script.
dnl

AC_DEFUN([MAKE_CLEAN], [
  INCLUDE_MAKEFILE([clean/Makefile])

  CLEAN_REGISTERED_TARGETS=""
  AC_SUBST([CLEAN_REGISTERED_TARGETS])
])

AC_DEFUN([REGISTER_CLEAN], [
  AS_IF([test "X[$1]" != "X"],
        [CLEAN_REGISTERED_TARGETS="${CLEAN_REGISTERED_TARGETS} [$1]clean"])
])
