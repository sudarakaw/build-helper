dnl  deploy/gitftp/init.m4: deploy using Git FTP
dnl
dnl  Copyright 2021 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify it
dnl  under the terms of the BSD 2-clause License. See the LICENSE file for more
dnl  details.
dnl
dnl  Process this file with autoconf to produce a configure script.
dnl

AC_DEFUN([DEPLOY_WITH_GITFTP], [

  Checks for git-ftp programs.
  AC_PATH_PROG([GITFTP], [git-ftp], [notfound])
  AS_IF([test "X$GITFTP" = "Xnotfound"],
        [AC_MSG_ERROR([git-ftp executable not found.])])

  AC_PATH_PROG([REALPATH], [realpath], [notfound])
  AS_IF([test "X$REALPATH" = "Xnotfound"],
        [AC_MSG_ERROR([realpath executable not found.])])

  dnl  Command line options
  AC_ARG_WITH([gitftp-target],
                [AS_HELP_STRING([--with-gitftp-target],
                                [SFTP or FTP@<:@E|ES@:>@ Url (default: ${DEPLOYTARGET})])],
                [GITFTPTARGET=${withval}],
                [GITFTPTARGET=${DEPLOYTARGET}])
  AC_SUBST([GITFTPTARGET])

  AS_IF([test "X${GITFTPTARGET}" = "X"],
        [AC_MSG_ERROR([Git FTP require target server URL set via either DEPLOYTARGET in global scope (i.e configure.ac) or using --with-gitftp-target])])

  AC_ARG_WITH([gitftp-user],
                [AS_HELP_STRING([--with-gitftp-user],
                                [Login name (default: ${DEPLOYUSER})])],
                [GITFTPUSER=${withval}],
                [GITFTPUSER=${DEPLOYUSER}])
  AC_SUBST([GITFTPUSER])

  AC_ARG_WITH([gitftp-syncroot],
                [AS_HELP_STRING([--with-gitftp-syncroot],
                                [(default: ${DEPLOYROOT})])],
                [GITFTPSYNCROOT=${withval}],
                [GITFTPSYNCROOT=${DEPLOYROOT}])
  AC_SUBST([GITFTPSYNCROOT])

  AC_ARG_ENABLE([gitftp-insecure],
              [AS_HELP_STRING([--enable-gitftp-insecure],
                              [Don't verify server's certificate])],
              [GITFTPINSECURE=yes],
              [GITFTPINSECURE=no])

  GITFTPFLAGS="--syncroot=`${REALPATH} --relative-to=${srcdir} ${GITFTPSYNCROOT}`/"
  AC_SUBST([GITFTPFLAGS])

  AS_IF([test "X${GITFTPUSER}" != "X"],
    [GITFTPFLAGS="${GITFTPFLAGS} --user=${GITFTPUSER}"])

  AS_IF([test "X${GITFTPINSECURE}" = "Xyes"],
    [GITFTPFLAGS="${GITFTPFLAGS} --insecure"])

  INCLUDE_MAKEFILE([deploy/gitftp/Makefile])

  REGISTER_DEPLOY([gitftp])

  MODULE_RESULT([Deploy (Git FTP)])
  MODULE_RESULT([  Executable      : '${GITFTP}'])
  MODULE_RESULT([  Options         : '${GITFTPFLAGS}'])
  MODULE_RESULT([  Target          : '${GITFTPTARGET}'])
  MODULE_RESULT([])
])
