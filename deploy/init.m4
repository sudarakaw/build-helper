dnl  deploy/init.m4: initialize generic deploy target
dnl
dnl  Copyright 2021 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify it
dnl  under the terms of the BSD 2-clause License. See the LICENSE file for more
dnl  details.
dnl
dnl  Process this file with autoconf to produce a configure script.
dnl

AC_DEFUN([USE_DEPLOY], [

  DEPLOY_REGISTERED_TARGETS=""
  AC_SUBST([DEPLOY_REGISTERED_TARGETS])

  AS_IF([test "X${DEBUG}" = "Xno"], [
    AS_IF([test "X$1" = "X"],
          [AC_MSG_ERROR([Deploy sub-system not specified])])

    AS_IF([test "X$1" = "Xgitftp"], [
      DEPLOY_WITH_GITFTP
    ])

    INCLUDE_MAKEFILE([deploy/Makefile])
  ])

  AC_SUBST([DEPLOYTARGET])
])

AC_DEFUN([REGISTER_DEPLOY], [
  AS_IF([test "X[$1]" != "X"],
        [DEPLOY_REGISTERED_TARGETS="${DEPLOY_REGISTERED_TARGETS} [$1]deploy"])
])
