# @configure_input@
#
# Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
#
# This program comes with ABSOLUTELY NO WARRANTY;
# This is free software, and you are welcome to redistribute it and/or modify it
# under the terms of the BSD 2-clause License. See the LICENSE file for more
# details.
#

# Substitution variables
BUILDHELPERDIR 	= @BUILDHELPERDIR@
CP 				= @CP@
MKDIR_P 		= @MKDIR_P@
TAR 			= @TAR@
ZIP 			= @ZIP@
tarname 		= @PACKAGE_TARNAME@
version 		= @PACKAGE_VERSION@
distdir 		= $(tarname)-$(version)
pkgdir 			= @pkgdir@
tarfile 		= $(pkgdir)/$(distdir).tar.gz

DIST_REGISTERED_TARGETS = @DIST_REGISTERED_TARGETS@

# Locate build script files & directories
BUILD_SYSTEM_FILES		= $(shell find $(BUILDHELPERDIR) -type f -name '*.m4' -o -name '*.in')
BUILD_SYSTEM_FILES	   := $(patsubst $(BUILDHELPERDIR)/%,$(distdir)/$(BUILDHELPERDIR)/%, $(BUILD_SYSTEM_FILES))
BUILD_SYSTEM_DIRS	   := $(sort $(dir $(BUILD_SYSTEM_FILES)))

.PHONY: dist
dist: $(tarfile)

$(tarfile): $(distdir) $(BUILD_SYSTEM_FILES) $(DIST_REGISTERED_TARGETS) $(pkgdir)
	$(CP) $(srcdir)/configure $(distdir)
	$(CP) $(srcdir)/configure.ac $(distdir)
	$(CP) $(srcdir)/install-sh $(distdir)
	$(CP) $(srcdir)/Makefile.in $(distdir)

	$(TAR) -chf - $< | $(ZIP) -9 -c > $@
	$(RM) -r $<

$(BUILD_SYSTEM_FILES): $(BUILD_SYSTEM_DIRS)
	$(CP) $(patsubst $(distdir)/%,%, $@) $@

.PHONY: $(pkgdir) $(BUILD_SYSTEM_DIRS)
$(pkgdir) $(BUILD_SYSTEM_DIRS):
	$(MKDIR_P) $@

.PHONY: FORCE_DIST
FORCE_DIST:
	-$(RM) $(tarfile) >/dev/null 2>&1
	$(RM) -r $(distdir)


