dnl  dist/init.m4: initialize dist target
dnl
dnl  Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify it
dnl  under the terms of the BSD 2-clause License. See the LICENSE file for more
dnl  details.
dnl
dnl  Process this file with autoconf to produce a configure script.
dnl

AC_DEFUN([MAKE_DIST], [
  INCLUDE_MAKEFILE([dist/Makefile])

  AC_PROG_MKDIR_P

  AC_PATH_PROG([CP], [cp], [notfound])
  AS_IF([test "X$CP" = "Xnotfound"],
        [AC_MSG_ERROR([cp executable not found.])])

  AC_PATH_PROG([TAR], [tar], [notfound])
  AS_IF([test "X$TAR" = "Xnotfound"],
        [AC_MSG_ERROR([tar executable not found.])])

  AC_PATH_PROG([ZIP], [gzip], [notfound])
  AS_IF([test "X$ZIP" = "Xnotfound"],
        [AC_MSG_ERROR([zip executable not found.])])

  pkgdir="${srcdir}/pkg"
  AC_SUBST([pkgdir])

  DIST_REGISTERED_TARGETS=""
  AC_SUBST([DIST_REGISTERED_TARGETS])
])

AC_DEFUN([REGISTER_DIST], [
  AS_IF([test "X[$1]" != "X"],
        [DIST_REGISTERED_TARGETS="${DIST_REGISTERED_TARGETS} [$1]dist"])
])
