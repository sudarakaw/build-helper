dnl  distcheck/init.m4: initialize distcheck target
dnl
dnl  Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify it
dnl  under the terms of the BSD 2-clause License. See the LICENSE file for more
dnl  details.
dnl
dnl  Process this file with autoconf to produce a configure script.
dnl

AC_DEFUN([MAKE_DISTCHECK], [
  INCLUDE_MAKEFILE([distcheck/Makefile])

  AC_PATH_PROG([FIND], [find], [notfound])
  AS_IF([test "X$FIND" = "Xnotfound"],
        [AC_MSG_ERROR([find executable not found.])])

  AC_PATH_PROG([WC], [wc], [notfound])
  AS_IF([test "X$WC" = "Xnotfound"],
        [AC_MSG_ERROR([wc executable not found.])])

  DISTCHECK_REGISTERED_TARGETS=""
  AC_SUBST([DISTCHECK_REGISTERED_TARGETS])
])

AC_DEFUN([REGISTER_DISTCHECK], [
  AS_IF([test "X[$1]" != "X"],
        [DISTCHECK_REGISTERED_TARGETS="${DISTCHECK_REGISTERED_TARGETS} [$1]distcheck"])
])
