dnl  distclean/init.m4: initialize distclean target
dnl
dnl  Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify it
dnl  under the terms of the BSD 2-clause License. See the LICENSE file for more
dnl  details.
dnl
dnl  Process this file with autoconf to produce a configure script.
dnl

AC_DEFUN([MAKE_DISTCLEAN], [
  INCLUDE_MAKEFILE([distclean/Makefile])

  DISTCLEAN_REGISTERED_TARGETS=""
  AC_SUBST([DISTCLEAN_REGISTERED_TARGETS])

  REGISTER_DISTCLEAN([internal])
])

AC_DEFUN([REGISTER_DISTCLEAN], [
  AS_IF([test "X[$1]" != "X"],
        [DISTCLEAN_REGISTERED_TARGETS="${DISTCLEAN_REGISTERED_TARGETS} [$1]distclean"])
])
