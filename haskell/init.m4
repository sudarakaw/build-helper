dnl  haskell/init.m4: Haskell support for development & testing
dnl
dnl  Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify
dnl  it under the terms of the BSD 2-clause License. See the LICENSE file for
dnl  more details.
dnl

AC_DEFUN([USE_HASKELL], [
  INCLUDE_MAKEFILE([haskell/Makefile])

  AC_CONFIG_FILES([${srcdir}/${PACKAGE_NAME}.cabal])

  AC_PATH_PROG([CABAL], [cabal], [notfound])
  AS_IF([test "X$CABAL" = "Xnotfound"],
        [AC_MSG_ERROR([cabal executable not found.])])

  AC_PATH_PROG([HLINT], [hlint], [notfound])
  AS_IF([test "X$HLINT" = "Xnotfound"],
        [AC_MSG_WARN([hlint executable not found. `make check` will not lint the source code.])])

  AC_PATH_PROG([XARGS], [xargs], [notfound])
  AS_IF([test "X$XARGS" = "Xnotfound"],
      [AC_MSG_ERROR([xargs executable not found.])])

  GHCOPTIONSCOMMON="-fhide-source-paths -Wall -Wcompat"

  GHCOPTIONSEXE="${GHCOPTIONSCOMMON}"
  AC_SUBST([GHCOPTIONSEXE])

  GHCOPTIONSLIB="${GHCOPTIONSCOMMON}"
  AC_SUBST([GHCOPTIONSLIB])

  AC_ARG_ENABLE([threads],
                [AS_HELP_STRING([--enable-threads],
                                [enable thread support])],
                [threads=yes],
                [threads=no])

  AS_IF([test "X$threads" = "Xyes"],
        [GHCOPTIONSEXE="${GHCOPTIONSEXE} -threaded -rtsopts -with-rtsopts=-N"]
        )

  AS_IF([test "X$DEBUG" = "Xno"],
        [CABALFLAGS="-O2"],
        [CABALFLAGS="-O0"])
  AC_SUBST([CABALFLAGS])

  REGISTER_BUILD([haskell])
  REGISTER_CHECK([haskell])
  REGISTER_CLEAN([haskell])
  REGISTER_DIST([haskell])
  REGISTER_DISTCLEAN([haskell])

  MODULE_RESULT([GHC/Cabal])
  MODULE_RESULT([  Executable      : ${CABAL} (build/test)])

  AS_IF([test "X${HLINT}" != "Xnotfound"],
  MODULE_RESULT([                  : ${HLINT} (lint)]))

  MODULE_RESULT([  Cabal Options   : ${CABALFLAGS}])
  MODULE_RESULT([  GHC Options     : ${GHCOPTIONSEXE} (exe) ])
  MODULE_RESULT([  GHC Options     : ${GHCOPTIONSLIB} (lib) ])
])
