dnl  init.m4: initialize the build system internals
dnl
dnl  Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify it
dnl  under the terms of the BSD 2-clause License. See the LICENSE file for more
dnl  details.
dnl
dnl  Process this file with autoconf to produce a configure script.
dnl

AC_DEFUN([BUILD_INIT], [
    AC_SUBST([BUILDHELPERDIR])

    dnl  Build mode (debug yes or no)
    AC_ARG_ENABLE([debug],
                [AS_HELP_STRING([--enable-debug],
                                [build in development mode])],
                [DEBUG=yes],
                [DEBUG=no])
    AC_SUBST([DEBUG])

    MODULE_RESULT_MESSAGE=""
    AC_SUBST([MODULE_RESULT_MESSAGE])

    UNIQE_MAKEFILE_INCLUDES=""
    AC_SUBST([UNIQE_MAKEFILE_INCLUDES])

    MAKE_BUILD
    MAKE_CHECK
    MAKE_CLEAN
    MAKE_DIST
    MAKE_DISTCHECK
    MAKE_DISTCLEAN
    MAKE_INSTALL
    MAKE_REPOCLEAN

    MODULE_RESULT([  Debug mode      : ${DEBUG}])
    MODULE_RESULT([])

])

AC_DEFUN([MODULE_RESULT], [MODULE_RESULT_MESSAGE="${MODULE_RESULT_MESSAGE}
[$1]"])

AC_DEFUN([INCLUDE_MAKEFILE], [
    m4_append_uniq([MK_FILES]
                  ,["${BUILDHELPERDIR}/$1"]
                  ,[ ]
                  ,[
                        UNIQE_MAKEFILE_INCLUDES="${UNIQE_MAKEFILE_INCLUDES} ${BUILDHELPERDIR}/$1"
                        AC_CONFIG_FILES([${BUILDHELPERDIR}/$1])
                   ]
                  )
])
