dnl  install/init.m4: initialize [un]install targets
dnl
dnl  Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify it
dnl  under the terms of the BSD 2-clause License. See the LICENSE file for more
dnl  details.
dnl
dnl  Process this file with autoconf to produce a configure script.
dnl

AC_DEFUN([MAKE_INSTALL], [
  INCLUDE_MAKEFILE([install/Makefile])

  AC_PROG_INSTALL
])
