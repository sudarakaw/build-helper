dnl  repoclean/init.m4: initialize repoclean target
dnl
dnl  Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify it
dnl  under the terms of the BSD 2-clause License. See the LICENSE file for more
dnl  details.
dnl
dnl  Process this file with autoconf to produce a configure script.
dnl

AC_DEFUN([MAKE_REPOCLEAN], [
    AC_PATH_PROG([GIT], [git], [notfound])

    AS_IF([test "X$GIT" = "Xnotfound"],
          [AC_MSG_WARN([repoclean target will not be available])],
          [
            INCLUDE_MAKEFILE([repoclean/Makefile])
          ])
])
