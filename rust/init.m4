dnl  rust/init.m4: Rust/Cargo support for development & testing
dnl
dnl  Copyright 2021 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify
dnl  it under the terms of the BSD 2-clause License. See the LICENSE file for
dnl  more details.
dnl

AC_DEFUN([USE_RUST], [
  RUST_PLUGINS="$1"

  INCLUDE_MAKEFILE([rust/Makefile])

  AC_CONFIG_FILES([${srcdir}/Cargo.toml])

  dnl  Checks for Cargo (build) programs.
  AC_PATH_PROG([CARGO], [cargo], [notfound])
  AS_IF([test "X$CARGO" = "Xnotfound"],
        [AC_MSG_ERROR([Cargo executable not found.])])


  dnl  Command line options
  AC_ARG_WITH([rust-target],
                [AS_HELP_STRING([--with-rust-target],
                                [Rust/Cargo build target (default: cargo or system default)])],
                [CARGOTARGET=${withval}],
                [CARGOTARGET=usedefault])

  dnl Attempt to get the default target via cargo
  AS_IF([test "X$CARGOTARGET" = "Xusedefault"],
        [CARGOTARGET=m4_esyscmd_s([cargo --version --verbose | grep '^host:' | cut -d' ' -f2])])

  AC_SUBST([CARGOTARGET])

  CARGOFLAGS="--manifest-path ${srcdir}/Cargo.toml"
  CARGOFLAGS="${CARGOFLAGS} --target=${CARGOTARGET}"
  AC_SUBST([CARGOFLAGS])

  AS_IF([test "X${DEBUG}" = "Xno"],
    [CARGOFLAGS="${CARGOFLAGS} --release"]
    [BUILDDIR="${srcdir}/target/${CARGOTARGET}/release"],
    [BUILDDIR="${srcdir}/target/${CARGOTARGET}/debug"])
  AC_SUBST([BUILDDIR])

  dnl Check for Tauri support
  AS_CASE([${RUST_PLUGINS}], [*tauri*], [USE_RUST_WITH_TAURI()])

  dnl Check for Trunk support
  AS_CASE([${RUST_PLUGINS}], [*trunk*], [USE_RUST_WITH_TRUNK()])

  dnl Checks for sqlx support.
  AS_CASE([${RUST_PLUGINS}], [*sqlx*], [USE_RUST_WITH_SQLX()])


  dnl Just Rust
  AS_CASE([${RUST_PLUGINS}],
    [*tauri*], [],
    [*trunk*], [],
    [REGISTER_BUILD([rust])])

  REGISTER_CHECK([rust])
  REGISTER_CLEAN([rust])
  REGISTER_DIST([rust])
  REGISTER_DISTCLEAN([rust])

  MODULE_RESULT([Rust/Cargo])
  MODULE_RESULT([  Executable      : ${CARGO} (build/test)])

  MODULE_RESULT([  Build flags     : ${CARGOFLAGS}])
  MODULE_RESULT([  Build dir       : ${BUILDDIR}])

  AS_IF([test "X${SQLX}" != "X"],
  MODULE_RESULT([])
  MODULE_RESULT([SQLx])
  MODULE_RESULT([  Executable      : ${SQLX}])
  )

  AS_IF([test "X${CARGOTAURI}" != "X"],
  MODULE_RESULT([])
  MODULE_RESULT([Tauri])
  MODULE_RESULT([  Executable      : ${CARGOTAURI}])
  MODULE_RESULT([  Build flags     : ${CARGOTAURIFLAGS}])
  MODULE_RESULT([  Icon path       : ${CARGOTAURIICONPATH}])
  MODULE_RESULT([  APK targets     : ${CARGOTAURIANDROIDTARGETS}])
  )

  AS_IF([test "X${TRUNK}" != "X"],
  MODULE_RESULT([])
  MODULE_RESULT([Trunk])
  MODULE_RESULT([  Executable      : ${TRUNK}])
  MODULE_RESULT([  Configuration   : ${TRUNKTOML}])
  )

  MODULE_RESULT([])

])
