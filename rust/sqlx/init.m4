dnl rust/sqlx/init.m4 : SQLx support for development & testing
dnl
dnl  Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify
dnl  it under the terms of the BSD 2-clause License. See the LICENSE file for
dnl  more details.
dnl

AC_DEFUN([USE_RUST_WITH_SQLX], [
  INCLUDE_MAKEFILE([rust/sqlx/Makefile])

  dnl Checks for SQLx binary.
  AC_PATH_PROG([SQLX], [sqlx], [notfound])
  AS_IF([test "X$SQLX" = "Xnotfound"],
        [AC_MSG_ERROR([SQLx executable not found.])])

])
