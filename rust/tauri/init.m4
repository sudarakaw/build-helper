dnl  rust/tauri/init.m4: Tauri support for development & testing
dnl
dnl  Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify
dnl  it under the terms of the BSD 2-clause License. See the LICENSE file for
dnl  more details.
dnl

AC_DEFUN([USE_RUST_WITH_TAURI], [
  INCLUDE_MAKEFILE([rust/tauri/Makefile])

  dnl  Checks for Tauri (build) programs.
  AC_PATH_PROG([CARGOTAURI], [cargo-tauri], [notfound])
  AS_IF([test "X$CARGOTAURI" = "Xnotfound"],
        [AC_MSG_ERROR([Cargo Tauri executable not found.])])

  dnl  Command line options
  AC_SUBST([CARGOTAURIICONPATH])

  ARGV=""
  AC_ARG_WITH([tauri-icons-path],
                [AS_HELP_STRING([--with-tauri-icons-path],
                                [Path to generate Tauri icons])],
                [CARGOTAURIICONPATH=${withval}],
                [ARGV="${srcdir}/icons"])
  CARGOTAURIICONPATH="${CARGOTAURIICONPATH:-$ARGV}"

  AS_IF([test "X$CARGOTAURIICONPATH" = "X"],
        [AC_MSG_ERROR([CARGOTAURIICONPATH is empty. Set it in configure.ac or ./configure command line.])])

  AC_SUBST([CARGOTAURIANDROIDTARGETS])

  ARGV=""
  AC_ARG_WITH([tauri-android-targets],
                [AS_HELP_STRING([--with-tauri-android-targets],
                                [Android build targets])],
                [CARGOTAURIANDROIDTARGETS=${withval}],
                [ARGV="aarch64"])
  CARGOTAURIANDROIDTARGETS="${CARGOTAURIANDROIDTARGETS:-$ARGV}"

  AS_IF([test "X$CARGOTAURIANDROIDTARGETS" = "X"],
        [AC_MSG_ERROR([CARGOTAURIANDROIDTARGETS is empty. Set it in configure.ac or ./configure command line.])])

  AC_SUBST([CARGOTAURICONFJSON])
  CARGOTAURICONFJSON=`${FIND} ${srcdir} -name "tauri.conf.json.in" -print -quit`
  if test -z "${CARGOTAURICONFJSON}"; then
    CARGOTAURICONFJSON="${srcdir}/tauri.conf.json"
  fi
  CARGOTAURICONFJSON="${CARGOTAURICONFJSON%%.in}"
  AC_CONFIG_FILES([${CARGOTAURICONFJSON}])

  CARGOTAURIFLAGS="--target=${CARGOTARGET}"
  AC_SUBST([CARGOTAURIFLAGS])

  AS_IF([test "X${DEBUG}" = "Xno"],
    [BUILDDIR="${srcdir}/target/tauri/${CARGOTARGET}/release"],
    [BUILDDIR="${srcdir}/target/tauri/${CARGOTARGET}/debug"])

  REQUIRE_RUNNER

  REGISTER_DISTCLEAN([tauri])
  REGISTER_BUILD([tauri])
  REGISTER_START([tauri])
  REGISTER_STOP([tauri])
])
