dnl  rust/trunk/init.m4: Trunk support for development & testing
dnl
dnl  Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify
dnl  it under the terms of the BSD 2-clause License. See the LICENSE file for
dnl  more details.
dnl

AC_DEFUN([USE_RUST_WITH_TRUNK], [
  INCLUDE_MAKEFILE([rust/trunk/Makefile])

  REQUIRE_HTTPD

  dnl  Checks for Trunk binary.
  AC_PATH_PROG([TRUNK], [trunk], [notfound])
  AS_IF([test "X$TRUNK" = "Xnotfound"],
        [AC_MSG_ERROR([Trunk executable not found.])])

  AC_SUBST([TRUNKTOML])
  TRUNKTOML=`${FIND} ${srcdir} -name "Trunk.toml.in" -print -quit`
  if test -z "${TRUNKTOML}"; then
    TRUNKTOML="${srcdir}/Trunk.toml"
  fi
  TRUNKTOML="${TRUNKTOML%%.in}"
  AC_CONFIG_FILES([${TRUNKTOML}])

  REGISTER_CLEAN([trunk])
  REGISTER_DIST([trunk])
  REGISTER_DISTCLEAN([trunk])
])
