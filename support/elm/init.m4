dnl  support/elm/init.m4: add Elm language support to build system
dnl
dnl  Copyright 2021 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify
dnl  it under the terms of the BSD 2-clause License. See the LICENSE file for
dnl  more details.
dnl

AC_DEFUN([REQUIRE_ELM], [
    m4_append_uniq([_INCLUDED_REQUIRED_MODULES], [ELM], [ ], [

      REQUIRE_NODEJS

      dnl  Checks for eml binary
      AC_PATH_PROGS([ELM], [elm], [notfound])
      AS_IF([test "X$ELM" = "Xnotfound"],
        [AC_MSG_ERROR([Elm binary not found.])])

      AC_PATH_PROGS([ELMTEST], [elm-test], [notfound])

      AC_PATH_PROGS([ELMREVIEW], [elm-review], [notfound])

      INCLUDE_MAKEFILE([support/elm/Makefile])

      REGISTER_CHECK([elm])

      MODULE_RESULT([Elm support])
      MODULE_RESULT([  Executable      : '${ELM}'])

      AS_IF([test "X${ELMREVIEW}" != "Xnotfound"],
          MODULE_RESULT([  Review          : yes]),
          MODULE_RESULT([  Review          : no]))

      AS_IF([test "X${ELMTEST}" != "Xnotfound"],
          MODULE_RESULT([  Test            : yes]),
          MODULE_RESULT([  Test            : no]))

      MODULE_RESULT([])

    ])
])
