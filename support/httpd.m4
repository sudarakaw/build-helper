dnl  support/httpd.m4: configure parameters & variables for HTTPD services
dnl
dnl  Copyright 2021 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify
dnl  it under the terms of the BSD 2-clause License. See the LICENSE file for
dnl  more details.
dnl

AC_DEFUN([REQUIRE_HTTPD], [
    m4_append_uniq([_INCLUDED_REQUIRED_MODULES], [HTTPD], [ ], [
        HTTPDPROGRAM=""
        AC_SUBST([HTTPDPROGRAM])

        dnl  Command line options
        AC_ARG_WITH([httpd-host],
            [AS_HELP_STRING([--with-httpd-host],
                [HTTP host (default: 127.0.0.1)])],
            [HTTPDHOST=${withval}],
            [HTTPDHOST=127.0.0.1])
        AC_SUBST([HTTPDHOST])

        AC_ARG_WITH([httpd-port],
            [AS_HELP_STRING([--with-httpd-port],
                [HTTP port (default: 5000)])],
            [HTTPDPORT=${withval}],
            [HTTPDPORT=5000])
        AC_SUBST([HTTPDPORT])
    ])
])
