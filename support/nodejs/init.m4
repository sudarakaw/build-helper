dnl  support/nodejs/init.m4: add Node.js support to build system
dnl
dnl  Copyright 2021 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify
dnl  it under the terms of the BSD 2-clause License. See the LICENSE file for
dnl  more details.
dnl

AC_DEFUN([REQUIRE_NODEJS], [
    m4_append_uniq([_INCLUDED_REQUIRED_MODULES], [NODEJS], [ ], [
      dnl  Checks for package manager for NodeJs (pnpm, npm or yarn)
      AC_PATH_PROGS([NODE_PACKAGE_MANAGER], [pnpm npm yarn], [notfound])
      AS_IF([test "X$NODE_PACKAGE_MANAGER" = "Xnotfound"],
        [AC_MSG_ERROR([Node.js package manager not found.])])

      INCLUDE_MAKEFILE([support/nodejs/Makefile])

      REGISTER_DISTCHECK([nodejs])

      MODULE_RESULT([Node.js support])
      MODULE_RESULT([  Package Manager : '${NODE_PACKAGE_MANAGER}'])
      MODULE_RESULT([])
    ])
])
