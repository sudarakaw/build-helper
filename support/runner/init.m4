dnl  runner/init.m4: initialize start/stop targets
dnl
dnl  Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify it
dnl  under the terms of the BSD 2-clause License. See the LICENSE file for more
dnl  details.
dnl
dnl  Process this file with autoconf to produce a configure script.
dnl

AC_DEFUN([REQUIRE_RUNNER], [
    m4_append_uniq([_INCLUDED_REQUIRED_MODULES], [RUNNER], [ ], [
        INCLUDE_MAKEFILE([support/runner/Makefile])

        START_REGISTERED_TARGETS=""
        STOP_REGISTERED_TARGETS=""

        AC_SUBST([START_REGISTERED_TARGETS])
        AC_SUBST([STOP_REGISTERED_TARGETS])
    ])
])

AC_DEFUN([REGISTER_START], [
    AS_IF([test "X[$1]" != "X"],
        [START_REGISTERED_TARGETS="${START_REGISTERED_TARGETS} [$1]start"])
])

AC_DEFUN([REGISTER_STOP], [
    AS_IF([test "X[$1]" != "X"],
        [STOP_REGISTERED_TARGETS="${STOP_REGISTERED_TARGETS} [$1]stop"])
])
