dnl  webpack/init.m4: Webpack(JS) for build & development web server
dnl
dnl  Copyright 2021 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify
dnl  it under the terms of the BSD 2-clause License. See the LICENSE file for
dnl  more details.
dnl

AC_DEFUN([USE_WEBPACK], [

  REQUIRE_NODEJS

  dnl  Checks for Webpack (build) programs.
  AC_PATH_PROG([WEBPACK], [webpack], [notfound])
  AS_IF([test "X$WEBPACK" = "Xnotfound"],
        [AC_MSG_ERROR([Webpack executable not found.])])

  AS_IF([test "X${BUILDDIR}" = "X"],
        [AC_MSG_ERROR([Webpack require BUILDDIR to be defined in global scope. (i.e configure.ac)])])

  INCLUDE_MAKEFILE([webpack/Makefile])

  REGISTER_DIST([webpack])

  WEBPACKFLAGS="--env VERSION=${PACKAGE_VERSION} --output-path=${BUILDDIR}"
  AC_SUBST([WEBPACKFLAGS])

  AS_IF([test "X${DEBUG}" = "Xno"],
    [WEBPACKFLAGS="${WEBPACKFLAGS} --env REVISION=\`git rev-parse --short HEAD\` --mode=production"],
    [WEBPACKFLAGS="${WEBPACKFLAGS} --env REVISION=\`cat /dev/urandom|head|sha1sum|cut -c -7\` --mode=development"])

  AS_CASE([$1],
    [*build*], [
      REGISTER_BUILD([webpack])

      INCLUDE_MAKEFILE([webpack/build.mk])
    ])

  MODULE_RESULT([Webpack])
  MODULE_RESULT([  Executable      : '${WEBPACK}'])
  MODULE_RESULT([  Options         : '${WEBPACKFLAGS}'])

  AS_IF([test "X${DEBUG}" = "Xyes"], [
    AS_CASE([$1],
      [*httpd*], [
        REQUIRE_HTTPD
        REQUIRE_RUNNER

        REGISTER_START([webpack])
        REGISTER_STOP([webpack])

        INCLUDE_MAKEFILE([webpack/httpd.mk])

        HTTPDPROGRAM="${WEBPACK} serve"

        MODULE_RESULT([  Web address     : 'http://${HTTPDHOST}:${HTTPDPORT}/'])
      ])
    ])

  MODULE_RESULT([])
])
