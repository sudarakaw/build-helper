dnl  wordpress/init.m4: Wordpress development support via LAMP on Docker
dnl
dnl  Copyright 2021 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify
dnl  it under the terms of the BSD 2-clause License. See the LICENSE file for
dnl  more details.
dnl

AC_DEFUN([USE_WORDPRESS], [

    AS_IF([test "X${DEBUG}" = "Xyes"], [
        dnl  Checks for Docker programs.
        AC_PATH_PROG([DOCKER], [docker], [notfound])
        AS_IF([test "X$DOCKER" = "Xnotfound"],
            [AC_MSG_ERROR([Docker executable not found.])])

        dnl  Checks for cUrl programs.
        AC_PATH_PROG([CURL], [curl], [notfound])

        dnl  Command line options
        AC_ARG_WITH([docker-container-prefix],
            [AS_HELP_STRING([--with-docker-container-prefix],
                [Name prefix of the docker containers. (default: ${PACKAGE_NAME})])],
            [DOCKER_CONTAINER_PREFIX=${withval}],
            [DOCKER_CONTAINER_PREFIX=${PACKAGE_NAME}])

        HTTPDDOCKERCONTAINER="${DOCKER_CONTAINER_PREFIX}-httpd-container"
        AC_SUBST([HTTPDDOCKERCONTAINER])
        DBDOCKERCONTAINER="${DOCKER_CONTAINER_PREFIX}-db-container"
        AC_SUBST([DBDOCKERCONTAINER])

        AS_IF([test "X$DEFAULT_DOCKER_IMAGE_HTTPD" = "X"],
            [DEFAULT_DOCKER_IMAGE_HTTPD=${PACKAGE_NAME}-httpd])

        AC_ARG_WITH([docker-image-httpd],
            [AS_HELP_STRING([--with-docker-image-httpd],
                [Name of the docker image to run HTTP server. (default: ${DEFAULT_DOCKER_IMAGE_HTTPD})])],
            [HTTPDDOCKERIMAGE=${withval}],
            [HTTPDDOCKERIMAGE=${DEFAULT_DOCKER_IMAGE_HTTPD}])
        AC_SUBST([HTTPDDOCKERIMAGE])

        AS_IF([test "X$DEFAULT_DOCKER_IMAGE_DB" = "X"],
            [DEFAULT_DOCKER_IMAGE_DB=${PACKAGE_NAME}-db])

        AC_ARG_WITH([docker-image-db],
            [AS_HELP_STRING([--with-docker-image-db],
                [Name of the docker image to run HTTP server. (default: ${DEFAULT_DOCKER_IMAGE_DB})])],
            [DBDOCKERIMAGE=${withval}],
            [DBDOCKERIMAGE=${DEFAULT_DOCKER_IMAGE_DB}])
        AC_SUBST([DBDOCKERIMAGE])

        AC_ARG_WITH([wordpress-db-dir],
                [AS_HELP_STRING([--with-wordpress-db-dir],
                    [Host directory to presist MariaDB database in docker (default: ${HOME}/opt/mysql/${PACKAGE_NAME})])],
                [WORDPRESS_DB_DIR=${withval}],
                [WORDPRESS_DB_DIR=${HOME}/opt/mysql/${PACKAGE_NAME}])
        AC_SUBST([WORDPRESS_DB_DIR])

        AC_ARG_WITH([wordpress-db-name],
                [AS_HELP_STRING([--with-wordpress-db-name],
                    [Name of the MariaDB database in docker (default: ${PACKAGE_NAME})])],
                [WORDPRESS_DB_NAME=${withval}],
                [WORDPRESS_DB_NAME=${PACKAGE_NAME}])
        AC_SUBST([WORDPRESS_DB_NAME])

        AC_ARG_WITH([wordpress-db-password],
                [AS_HELP_STRING([--with-wordpress-db-password],
                    [root password for MariaDB in docker (default: ${PACKAGE_NAME})])],
                [WORDPRESS_DB_PASSWORD=${withval}],
                [WORDPRESS_DB_PASSWORD=${PACKAGE_NAME}])
        AC_SUBST([WORDPRESS_DB_PASSWORD])

        DOCKERFLAGS="-d --rm"
        AC_SUBST([DOCKERFLAGS])

        REQUIRE_HTTPD
        REQUIRE_RUNNER

        REGISTER_CLEAN([wordpress])
        REGISTER_START([wordpress])
        REGISTER_STOP([wordpress])

        INCLUDE_MAKEFILE([wordpress/Makefile])

        MODULE_RESULT([Docker])
        MODULE_RESULT([  Executable      : '${DOCKER}'])
        MODULE_RESULT([  Options         : '${DOCKERFLAGS}'])
        MODULE_RESULT([  Web server      : '${HTTPDDOCKERIMAGE} (image)'])
        MODULE_RESULT([                  : '${HTTPDDOCKERCONTAINER} (container)'])
        MODULE_RESULT([  DB server       : '${DBDOCKERIMAGE} (image)'])
        MODULE_RESULT([                  : '${DBDOCKERCONTAINER} (container)'])
        MODULE_RESULT([])

        MODULE_RESULT([Wordpress])
        MODULE_RESULT([  DB name         : '${WORDPRESS_DB_NAME}'])
        MODULE_RESULT([  DB password     : '${WORDPRESS_DB_PASSWORD}'])
        MODULE_RESULT([  DB directory    : '${WORDPRESS_DB_DIR}'])
        MODULE_RESULT([  Web address     : 'http://${HTTPDHOST}:${HTTPDPORT}/'])
    ])
])
