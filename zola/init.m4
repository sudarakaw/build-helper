dnl  zola/init.m4: Zola for build & development web server
dnl
dnl  Copyright 2021 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
dnl
dnl  This program comes with ABSOLUTELY NO WARRANTY;
dnl  This is free software, and you are welcome to redistribute it and/or modify
dnl  it under the terms of the BSD 2-clause License. See the LICENSE file for
dnl  more details.
dnl

AC_DEFUN([USE_ZOLA], [

  dnl  Checks for Zola programs.
  AC_PATH_PROG([ZOLA], [zola], [notfound])
  AS_IF([test "X$ZOLA" = "Xnotfound"],
        [AC_MSG_ERROR([Zola executable not found.])])

  AS_IF([test "X${BUILDDIR}" = "X"],
        [AC_MSG_ERROR([Zola require BUILDDIR to be defined in global scope. (i.e configure.ac)])])

  INCLUDE_MAKEFILE([zola/Makefile])

  ZOLAFLAGS="--root=${srcdir}/src"
  AC_SUBST([ZOLAFLAGS])

  ZOLABUILDFLAGS="--output-dir=${BUILDDIR}"
  AC_SUBST([ZOLABUILDFLAGS])

  REGISTER_BUILD([zola])

  MODULE_RESULT([Zola])
  MODULE_RESULT([  Executable      : '${ZOLA}'])
  MODULE_RESULT([  Options         : '${ZOLAFLAGS}'])
  MODULE_RESULT([                  : '${ZOLABUILDFLAGS} (build)'])

  AS_IF([test "X${DEBUG}" = "Xyes"], [
    INCLUDE_MAKEFILE([zola/httpd.mk])

    REQUIRE_HTTPD
    REQUIRE_RUNNER

    REGISTER_START([zola])
    REGISTER_STOP([zola])

    HTTPDPROGRAM="${ZOLA}"

    AC_PATH_PROG([MKTEMP], [mktemp], [notfound])
    AS_IF([test "X$MKTEMP" = "Xnotfound"],
          [AC_MSG_ERROR([mktemp executable not found.])])

    ZOLASERVERBUILDDIR="$(${MKTEMP} --directory --dry-run)"
    AC_SUBST([ZOLASERVERBUILDDIR])

    ZOLASERVERFLAGS="--output-dir=${ZOLASERVERBUILDDIR}"
    ZOLASERVERFLAGS="${ZOLASERVERFLAGS} --fast"
    ZOLASERVERFLAGS="${ZOLASERVERFLAGS} --interface=${HTTPDHOST}"
    ZOLASERVERFLAGS="${ZOLASERVERFLAGS} --port=${HTTPDPORT}"
    AC_SUBST([ZOLASERVERFLAGS])

    MODULE_RESULT([                  : '${ZOLASERVERFLAGS} (server)'])
    MODULE_RESULT([  Web address     : 'http://${HTTPDHOST}:${HTTPDPORT}/'])
  ])

  MODULE_RESULT([])
])
